# Implementacija (poboljšanog) Canny-jevog algoritma za detekciju ivica
Projekat za predmet digitalna obrada slike (osnove kompjuterske vizije) realizovan u python-u.
Implementira se algoritam koji se zasniva na Canny-jevom algoritmu. Sadrži dodatne operacije, kao što su morfološko filtriranje, izračunavanje gradijenta, dvostruko pragovanje, itd...
### Detaljan opis metode u dokumentu:
![izvestaj](Izvestaj-Agnes-Pap.pdf)

