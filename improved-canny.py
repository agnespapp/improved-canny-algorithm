from skimage import morphology, io, color
import numpy as np
from scipy import ndimage
import math

def rgb_check(img):

    color_indicator = len(img.shape)

    if color_indicator == 3:
        gray_image = 255 * color.rgb2gray(img)
    else:
        gray_image = img

    return gray_image

def add_noise(img,p0):

    rng = np.random.default_rng()
    p = rng.uniform(size=img.shape)

    img_noisy = img.copy()

    img_noisy[p<p0/2] = 0
    img_noisy[(p>=p0/2)&(p<p0)] = 255

    return img_noisy

def morphological_filtering(img_noisy, a, b):

    A = morphology.diamond(a)
    B = morphology.diamond(b)

    img_filtered = morphology.closing(morphology.opening(img_noisy,A),B)

    return img_filtered

def gradient_xy(img_filtered):

    gx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
    gy = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])

    Gx = ndimage.convolve(img_filtered.astype('float'), gx, mode='mirror')
    Gy = ndimage.convolve(img_filtered.astype('float'), gy, mode='mirror')

    Gxy = np.abs(Gx) + np.abs(Gy)
    Txy = np.rad2deg(np.arctan(Gy / (Gx + np.finfo('float').eps)))

    return Gxy, Txy

def gradient_diagonal(img_filtered):

    g45 = np.array([[0,1,2],[-1,0,1],[-2,-1,0]])
    g135 = np.array([[-2,-1,0],[-1,0,1],[0,1,2]])

    G45 = ndimage.convolve(img_filtered.astype('float'), g45, mode='mirror')
    G135 = ndimage.convolve(img_filtered.astype('float'), g135, mode='mirror')

    Gdiagonal = np.abs(G45) + np.abs(G135)
    Tdiagonal= np.rad2deg(np.arctan(G135 / (G45 + np.finfo('float').eps)))

    return Gdiagonal, Tdiagonal

def theta_quantization(theta):

    theta_quantized = np.zeros(theta.shape)

    for i in range(theta.shape[0]):
        for j in range(theta.shape[1]):

            if((theta[i,j] >= -22.5 and theta[i,j] < 22.5 ) or (theta[i,j] >= 157.5 and theta[i,j] < -157.5)):
                theta_quantized[i,j] = 0

            elif((theta[i,j] >= 22.5 and theta[i,j] < 67.5) or (theta[i,j] < -112.5 and theta[i,j] >= -157.5)):
                theta_quantized[i,j] = 135

            elif ((theta[i, j] >= 67.5 and theta[i, j] < 112.5) or (theta[i, j] < -67.5 and theta[i, j] >= -112.5 )):
                theta_quantized[i, j] = 90

            else:
                theta_quantized[i, j] = 45

    return theta_quantized

def nonmaxima_suppression(kernel, g1, theta_quantized):

    pad_row = math.floor(kernel.shape[0] / 2)
    pad_col = math.floor(kernel.shape[1] / 2)

    g1_pad = np.pad(g1, ((pad_row,), (pad_col,)),mode='symmetric')

    out_nonmax = np.zeros(g1.shape)

    for row in range(out_nonmax.shape[0]):
        for col in range(out_nonmax.shape[1]):

            if ((g1_pad[row:row + kernel.shape[0], col:col + kernel.shape[1]]).shape == kernel.shape):
                region_grad = g1_pad[row:row + kernel.shape[0], col:col + kernel.shape[1]]

            if (theta_quantized[row,col] == 0):
                if ((region_grad[1, 1] < region_grad[1, 0]) or (region_grad[1, 1] < region_grad[1, 2])):
                    out_nonmax[row, col] = 0
                else:
                    out_nonmax[row, col] = region_grad[1, 1]

            if (theta_quantized[row,col] == 45):
                if ((region_grad[1, 1] < region_grad[0, 2]) or (region_grad[1, 1] < region_grad[2, 0])):
                    out_nonmax[row, col] = 0
                else:
                    out_nonmax[row, col] = region_grad[1, 1]

            if (theta_quantized[row,col] == 90):
                if ((region_grad[1, 1] < region_grad[0, 1]) or (region_grad[1, 1] < region_grad[2, 1])):
                    out_nonmax[row, col] = 0
                else:
                    out_nonmax[row, col] = region_grad[1, 1]

            if (theta_quantized[row,col] == 135):
                if ((region_grad[1, 1] < region_grad[0, 0]) or (region_grad[1, 1] < region_grad[2, 2])):
                    out_nonmax[row, col] = 0
                else:
                    out_nonmax[row, col] = region_grad[1, 1]

    return out_nonmax

def morph_connect(img,x,y,connectivity):

    X_k0 = np.full(img.shape, False)
    X_k0[x,y] = 1

    if(connectivity==4):
        B = morphology.diamond(1)
    else:
        B = morphology.square(3)

    not_equal = True
    while not_equal:
        Xk = (morphology.dilation(X_k0, B)) & img.astype('uint8')
        if (Xk == X_k0).all():
            not_equal = False
        else:
            X_k0 = Xk

    return X_k0

def double_thresold(G, low_ratio, high_ratio,connectivity):

    diff = np.max(G) - np.min(G)
    T_low = np.min(G) + low_ratio * diff
    T_high = np.min(G) + high_ratio * diff

    strong_row, strong_col = np.where(G >= T_high)
    candidate_row, candidate_col = np.where((G >= T_low) & (G < T_high))
    weak_row, weak_col = np.where(G < T_low)

    G[weak_row, weak_col] = 0
    G[strong_row, strong_col] = 1
    G[candidate_row, candidate_col] = 1

    img_edge = np.zeros(G.shape)

    for i in range(len(strong_row)):
        if not img_edge[strong_row[i], strong_col[i]] == 1:
            img_edge += morph_connect(G, strong_row[i], strong_col[i],connectivity)

    return img_edge

def improved_canny(image_noisy, kernel, low_threshold_ratio, high_threshold_ratio, connectivity):

    #1. Morfolosko filtriranje
    img_filtered = morphological_filtering(image_noisy,1,2)

    #2. Gradijenti slike
    g1, theta1 = gradient_xy(img_filtered)
    g2, theta2 = gradient_diagonal(img_filtered)

    #3. Non-maxima suppression
    theta1_quantized = theta_quantization(theta1)
    theta2_quantized = theta_quantization(theta2)
    G1N = nonmaxima_suppression(kernel,g1,theta1_quantized)
    G2N = nonmaxima_suppression(kernel,g2,theta2_quantized)

    #4. GN
    GN = np.maximum(G1N,G2N)

    #5. Dvostruko pragovanje
    image_edge = double_thresold(GN, low_threshold_ratio, high_threshold_ratio, connectivity)

    return image_edge

image = io.imread('f02_dfh_ax.jpg')
image = rgb_check(image)
image_noisy = add_noise(image,0.1)

image_edge = improved_canny(image_noisy,kernel=np.ones((3,3)),low_threshold_ratio=0.07,high_threshold_ratio=0.14,connectivity=8)
io.imsave('edge.png',image_edge)

